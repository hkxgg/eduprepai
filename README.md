# <h1 style="font-size: 36px;">EduPrep AI 🤖</h1>

<div align="center">
  <img src="/uploads/1f9ad9e7e522df525d1480d704c8c9af/EduPrepAI.png" width="640" height="430">
</div>

---

## Problem Research 📚

We conducted extensive research and found that students often struggle to find relevant practice questions, leading to a lack of practice, while teachers face challenges in creating personalized content. Students tend to rely on summary videos for exam preparation, which may not sufficiently cover the material. To address these issues, we propose a solution that offers quick summarization, interactive quizzes, and personalized content generation based on educational videos.


## Problem Statement 🎯

- Watching lengthy lecture videos is time-consuming and tedious.
- The Students struggle to find relevant questions for practice.
- Teachers spend considerable time creating quizzes and notes manually.
- Limited access to personalized content affects students' exam preparation.
- Students with learning disabilities, such as those with attention disorders face difficulties.

---

## Solution Overview 💡

Our innovative EdTech platform aims to enhance the learning experience by converting educational video lectures into summary, lecture notes and engaging MCQ-based quizzes and providing automatic assessment. This project not only streamlines the quiz creation process for teachers but also offers students a dynamic and interactive learning environment. With features such as instant grading and performance analytics, our platform facilitates efficient and effective learning.

### Key Features: 🚀
- Quick Summarization of Educational Videos
- Interactive Quizzes Based on Video Content
- Automatic Assessment and Performance Analytics
- Notes Generation of a Lecture
- Chat and Query any educational Video
 <br><br>

## Project Architecture 
<div align="center">
  <img src="/uploads/f858105feecb9ce8f545da51a5777a74/arch_edu.png">
</div>
<br><br>
At a high level, it parses both the visual and auditory components of the video, extracts text from each, combines them, and then summarizes the combined text using automatic summarization methods. 
<br><br>

<div align="center">
  <img src="/uploads/a8a7a9fe924c9bac5a87e9d77e5d4761/ocr_important.png">
</div>

---

## Future Additions ⛏️

- AI-generated Notes From PDFs, PowerPoints, and Lecture Videos [implemented for video]
- AI Tutor Chat Support for Studying
- Search Functionality within Lecture Videos [implemented]
- Improved Quiz Generation Algorithms [implemented]

---

## Roadmap 🛣️

1. **Scraping Data** [Successful]

2. **Data Collection and Selection** [Successful]
    - Utilize educational videos from open access university websites and YouTube

3. **Frame Extraction and Operations** [Successful]
    - Extract frames using cv2
    - Removing the duplicate frames using MSE

4. **Labeling Dataset** [Successful]
    - Label frames as slide, presenter slide, and others

5. **Model Training** [Successful] - improving
    - Base Models: Resnet50, YOLO v9, EAST, LLama2-7b
    - Future Scope: Explore more state-of-the-art models

6. **Image Tuning Operations** [Successful]
    - Crop Transform, Border Removal, Image Hashing

7. **Sequential Data Context Search** [Successful]
    - It processes a sequence of images, comparing them based on both visual similarity (using SSIM) and textual similarity (using Levenshtein distance) and saves images that are dissimilar enough to the output folder.

**eg: Here, we just need the frame 4 as the frames [1,2,3] are the pre sequential ones and have less information.**
[1,2,3 ⊆ 4]

| 1                                           | 2                                              | 3                                           | 4                                              |
|-------------------------------------------------------------|-------------------------------------------------------------|-------------------------------------------------------------|-------------------------------------------------------------|
| <img src="/uploads/9228f6440ce8d7c77da00f0f8d853423/frame_67.jpg" alt="Original Image 1" width="200"> | <img src="/uploads/30ef2b8495a07b0aaefac7dbd5e6dad8/frame_73.jpg" alt="Masked Image 1" width="200"> | <img src="/uploads/ec446861daf075003b50a6bd6cab9dea/frame_85.jpg" width="200"> | <img src="/uploads/67181224e13243589d7ed83d4a26b6d6/frame_96.jpg" alt="Masked Image 2" width="200"> |

<br>

8. **OCR, Figure Detection** [Successful]

    - Using PIL, CV, Contour Detection for annotations and making bounding boxes for figures or diagrams having border or no border. It utilizes JSON serialization to store bounding box coordinates and areas in structured format.
    - Using JSON, we calculate average area of bounding boxes for each frame and return those boxes having `area > 2.5 of average area`.
    - Masking the figure to get more accurate text.

| OCR Image                                              | Masked Image                                                |
|-------------------------------------------------------------|-------------------------------------------------------------|
| <img src="/uploads/eddff9fc967b8e84c0a0f5dc1a72d33b/frame_90.jpg" alt="Original Image" width="400"> | <img src="/uploads/db352ab564fc0943a31522357f9d0676/WhatsApp_Image_2024-04-06_at_00.14.26_179897a0.jpg" alt="Masked Image" width="400"> |


9. **Transcription (Multimodal)** [Successful]
    - Audio to Transcript Generation - using Whisper

10. **Combination and Sync** [Successful] - improving

11. **Text Operations** [Successful]
    - Spell Check (symspell), etc.

12. **Summarization and Quiz Generation** [Successful]
    - Utilize LLama2 finetuning and pre-existing models like BERT-edu
    - Answer Key/Options Generation using Sense2vec word vectors

Using **Refine** chain type for token limit workaround.
<div align="center">
  <img src="/uploads/9d9578f3246fddff52d87d51926cc74a/Screenshot_2024-04-10_173446.png">
</div>

<br>

13. **Chat with Video** [Successful]
    - Interactive: You can ask any question relevant to the video content, and it will provide a concise answer.
    - Utilizes NLP techniques: It uses the OpenAI GPT-3.5 language model to process and answer questions.
    - Search Functionality within Lecture Videos: Each answer gives you a timestamp link to the relevant clip from the video, making it easy to refer back to the original context.
---

## Dataset 🔖

We have scraped educational lecture videos from open access university websites and youtube: <br> [Videos Dataset Link](https://drive.google.com/file/d/1PGcngz276xWW82c2JycLrNkuqNY5gsPL/view?usp=sharing)

Slide classifier dataset which was made by labeling the frames into 3 categories: slide, presenter_slide and other. <br>
[Model Training Data for Slide Classifier
](https://drive.google.com/file/d/1NcVuXvYvLFEuXg52gbi_PTfUj7s1ur2S/view?usp=sharing)

[Figure Detection Dataset for YOLO Model](https://drive.google.com/file/d/1yoilKif2bBb8Z9j8D5yqQKmjzG0uS7IB/view)




---

## Models

Base / PreTrained Models Used: Resnet50, YOLO v9, EAST, LLama2-7b

1) The slide classification model (Resnet50) is used to classify frames of an input video into 3 classes:
 - slide <br>
 - presenter_slide <br>
 - other

**i.e: Here, slide class contains the best frames, presenter_slide contain the frames with information but need modification (perspective cropping of the lecturer) and the other class is the one which contain waste frames. (lecturer blocking the text view)**

| slide                                              | presenter_slide                                                | other                                                 |
|-------------------------------------------------------------|-------------------------------------------------------------|-------------------------------------------------------------|
| <img src="/uploads/9d647ced83c810815bf73a9772151c91/8ddf851b52f-img_169.jpg" alt="Original Image" width="300"> | <img src="/uploads/849ddc1779124f002698d406ed82a4c4/_Qai4RxTJoM-img_036.jpg" alt="Masked Image" width="270"> | <img src="/uploads/7dbd99a578ac1854d399c38bbf8b8888/_TEMQhpsGFg-img_159.jpg" alt="Third Image" width="300"> |

 
This allows to identify images containing slides for additional processing and means it can ignore useless frames that do not belong in a summary, such as those containing only the presenter, audience or a blockage for text.
After testing several architectures, we chose ResNet-50 as our final model architecture due to its speed, accuracy, and size. 

We started training all models from ImageNet pre-trained checkpoints and perform gradient updates on the last chunk of layers (the pooling and fully connected linear layers). 

| Layer                          | Description             |
|--------------------------------|-------------------------|
| AdaptiveConcatPool2d(1)        | Adaptive concatenation of max and average pooling |
| Flatten()                      | Flattens the input tensor |
| BatchNorm1d(1024)              | Batch normalization for 1024 channels |
| Dropout(0.25)                  | Dropout with a probability of 25% |
| Linear(1024, 512)              | Fully connected layer mapping 1024 to 512 units |
| ReLU(inplace=True)             | Rectified Linear Unit activation function |
| BatchNorm1d(512)               | Batch normalization for 512 channels |
| Dropout(0.5)                   | Dropout with a probability of 50% |
| Linear(512, num_classes)       | Fully connected layer mapping 512 to num_classes units |


The highest average accuracy of 85.42% was achieved by a ResNet-50 model after training for 9 epochs with the following hyperparameters: batch_size=64, learning_rate=0.00478, momentum=0.952, weight_decay=0.00385, adamw_alpha=0.994, adamw_eps=4.53e-07, scheduler=onecycle.

<div align="center">
  <img src="/uploads/6c42bcb1ac1938a640c8180fa2e5cb57/Screenshot_2024-04-05_102029.png">
</div>
<br><br>
<div align="center">
  <img src="/uploads/cd9ee4316bd528dad912fe2e9072cea5/Screenshot_2024-04-05_102528.png">
</div><br>

2) Training YOLOv9 on slide figure dataset for figures/flowcharts detection in the video.

3) Finetuning pretrained EAST on the slides for an efficient and accurate scene text detection (https://github.com/argman/EAST)

4) Text Summarization & Operations using Llama2-7b + RAG (clarifai)


>prompt = `"The text provided above is a obtained text from an educational video lecture. Your job is to act as a teacher, creating a test using the transcribed video lecture text. The test should contain only multiple choice questions with 4 possible answers to each question and there need to be 10 questions in total. The test is to be made using the information provided only in the transcribed text. You are not supposed to create questions using context outside of the transcribed test. You must also not provide false information as it can ruin the academic integrity of the institution that you are employed at.` <br>
`Your response must be in the form of an array, where the 0th index is the question number, 1st index is the question, 2nd index is the first choice, 3rd index is the second choice, 4th index is the third choice, 5th index is the fourth choice, 6th index is the correct answer. Below provided is an example array that you must give your response as. You are supposed to only provide me the response in accordance to the format stated below.` <br>
`An example array:
[[1, "What is the capital of India?", "Kolkata", "Bengaluru", "New Delhi", "Mumbai", "New Delhi"], [2, "How many kidneys does a human have?", "1", "2", "3", "4", "2"]]` <br><br>
`Give me the result in the form of an array as stated in my question above."`


---

## Features ✨

#### Multimodal
#### Quick Summarization of Lecture Videos
#### Interactive Quizzes to Enhance Learning and Practice
#### Personalized Content based on Lecture Topics
#### Customizable Question Types and Parameters

---

## Future Scope 🔭

- Incorporate Natural Language Understanding (NLU) models for assessment
- Database Integration for improved efficiency
- Enhanced Security measures for user data protection
- Development of an internal AI assistant for navigation
- Understanding the images in the video
- Saves Time and Efforts (can be used as a browser extension)

---

## Demo Video 🎥

Check out our demo video to see EduPrep AI in action:

https://youtu.be/ikYq7MhHAW8?si=30Fqnghp9Om54ggK

## Presentation 📑

http://surl.li/rqmik
