import logging
import os
import shutil

import cv2
import numpy as np
from tqdm import tqdm

logger = logging.getLogger(__name__)

OUTPUT_PATH_MODIFIER = "_noborder"


def does_image_have_border(image, gamma=5):
    """Detect if an image has a solid black border.

    Args:
        image (np.array): An image loaded using ``cv2.imread()``.
        gamma (int, optional): How far the pixel values can vary before they are
            considered not black. This is useful if the image contains
            noise. Defaults to 5.

    Returns:
        bool: If all pixel values on any or all sides of the image are
        black +/- ``gamma``.
    """
    if gamma == 0:
        gamma = 1

    left_edge = (image[:, 0] < gamma).all()
    right_edge = (image[:, -1] < gamma).all()
    top_edge = (image[0, :] < gamma).all()
    bottom_edge = (image[-1, :] < gamma).all()
    if left_edge or right_edge or top_edge or bottom_edge:
        return True
    return False


def detect_solid_color(image, gamma=5):
    """Detect if an image contains a solid color.

    Args:
        image (np.array): An image loaded using ``cv2.imread()``.
        gamma (int, optional): How far the pixel values can vary before they are
            considered a different color. This is useful if the image contains
            noise. Defaults to 5.

    Returns:
        bool: If all pixel values are the same +/- ``gamma``.
    """
    flattened_image = np.ravel(image)
    first_value = flattened_image[0]
    all_values_equal = ((first_value - gamma) < flattened_image).all() and (
        flattened_image < (first_value + gamma)
    ).all()
    return all_values_equal


def remove_border(image_path, output_directory=None):
    image = cv2.imread(image_path)

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    solid_color = detect_solid_color(gray)
    if solid_color:
        return None

    if not output_directory:
        file_parse = os.path.splitext(str(image_path))
        filename = file_parse[0]
        ext = file_parse[1]
        output_path = filename + OUTPUT_PATH_MODIFIER + ext
    else:
        output_path = os.path.join(output_directory, os.path.basename(image_path))

    image_has_border = does_image_have_border(gray)
    if not image_has_border:
        shutil.copyfile(image_path, output_path)
        return output_path

    blur_amount = 3
    blurred = cv2.GaussianBlur(gray, (blur_amount, blur_amount), 0)
    _, thresh = cv2.threshold(blurred, 5, 255, cv2.THRESH_BINARY)

    contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    height = image.shape[0]
    width = image.shape[1]
    min_area = height * width * 0.7
    max_area = (width - 15) * (height - 15)
    final_contour = None
    for cnt in contours:
        perimeter = cv2.arcLength(cnt, True)
        approx = cv2.approxPolyDP(cnt, 0.1 * perimeter, True)

        if (
            len(approx) == 4
            and cv2.isContourConvex(approx)
            and min_area < cv2.contourArea(approx) < max_area
        ):
            min_area = cv2.contourArea(approx)
            final_contour = approx[:, 0]

    if final_contour is None:
        return image_path

    x, y, w, h = cv2.boundingRect(final_contour)

    crop = image[
        y + blur_amount : y + h - blur_amount, x + blur_amount : x + w - blur_amount
    ]
    cv2.imwrite(output_path, crop)

    return output_path


def all_in_folder(path, remove_original=False, **kwargs):
    removed_border_paths = []
    images = os.listdir(path)
    images.sort()

    for item in tqdm(images, total=len(images), desc="> Border Removal: Progress"):
        current_path = os.path.join(path, item)
        if os.path.isfile(current_path) and OUTPUT_PATH_MODIFIER not in str(
            current_path
        ):
            output_path = remove_border(current_path, **kwargs)
            if output_path is None:
                continue

            removed_border_paths.append(output_path)
            if remove_original:
                os.remove(current_path)
    logger.debug("> Border Removal: Returning removed border paths")
    return removed_border_paths


if __name__ == "__main__":
    input_image_folder = r"C:\Users\HARSHITA KAMANI\Desktop\l2n\frames_sorted_1\slide"
    removed_border_paths = all_in_folder(input_image_folder, remove_original=True)

    print("Processed images saved at:", removed_border_paths)