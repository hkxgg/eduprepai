import argparse
import logging
import os

import cv2
import imageio
import numpy as np
from pygifsicle import optimize
from tqdm import tqdm

from helpers import make_dir_if_not_exist

logger = logging.getLogger(__name__)

OUTPUT_PATH_MODIFIER = "_cropped"


def resize(img, height=800, allways=False):
    if img.shape[0] > height or allways:
        rat = height / img.shape[0]
        return cv2.resize(img, (int(rat * img.shape[1]), height))

    return img


def find_intersection(line1, line2):
    x1, y1, x2, y2 = line1[0]
    x3, y3, x4, y4 = line2[0]
    Px = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / (
        (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
    )
    Py = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / (
        (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
    )
    return Px, Py


def segment_lines(lines, delta):
    h_lines = []
    v_lines = []
    for line in lines:
        for x1, y1, x2, y2 in line:
            if abs(x2 - x1) < delta: 
                v_lines.append(line)
            elif abs(y2 - y1) < delta:  
                h_lines.append(line)
    return h_lines, v_lines


def cluster_points(points, nclusters):
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    _, _, centers = cv2.kmeans(
        points, nclusters, None, criteria, 10, cv2.KMEANS_PP_CENTERS
    )
    return centers


def remove_contours(edges, contour_removal_threshold):
    result = np.full_like(edges, 0, dtype="uint8")
    contours, hierarchy = cv2.findContours(
        edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
    )

    contours = [
        cnt for cnt in contours if cv2.arcLength(cnt, True) > contour_removal_threshold
    ]

    for c in contours:
        result = cv2.drawContours(result, [c], -1, (255, 255, 255), 2)

    return result


def hough_lines_corners(
    img, edges_img, min_line_length, border_size=11, debug_output_imgs=None
):
    edges_img = edges_img[border_size:-border_size, border_size:-border_size]
    height = img.shape[0]

    lines = cv2.HoughLinesP(
        edges_img,
        rho=1,
        theta=np.pi / 180,
        threshold=100,
        maxLineGap=int(height / 54),
        minLineLength=min_line_length,
    )
    if lines is None:
        logger.debug(
            "Hough Lines Corners Failed! `cv2.HoughLinesP` failed to find any lines."
        )
        return None
    h_lines, v_lines = segment_lines(lines, int(height / 54))

    Px = []
    Py = []
    for h_line in h_lines:
        for v_line in v_lines:
            px, py = find_intersection(h_line, v_line)
            Px.append(px)
            Py.append(py)

    if len(Px) <= 4:
        logger.debug(
            "Hough Lines Corners Failed! Only "
            + str(len(Px))
            + " intersection points found."
        )
        return None

    P = np.float32(np.column_stack((Px, Py)))
    nclusters = 4 
    centers = cluster_points(P, nclusters)

    if debug_output_imgs is not None:
        hough_img = img.copy()
        for line in h_lines:
            for x1, y1, x2, y2 in line:
                color = [0, 0, 255] 
                cv2.line(hough_img, (x1, y1), (x2, y2), color=color, thickness=1)
        for line in v_lines:
            for x1, y1, x2, y2 in line:
                color = [255, 0, 0] 
                cv2.line(hough_img, (x1, y1), (x2, y2), color=color, thickness=1)

        intersects_img = img.copy()
        for cx, cy in zip(Px, Py):
            cx = np.round(cx).astype(int)
            cy = np.round(cy).astype(int)
            color = np.random.randint(0, 255, 3).tolist() 
            cv2.circle(
                intersects_img, (cx, cy), radius=2, color=color, thickness=-1
            )

        corners_img = img.copy()
        for cx, cy in centers:
            cx = np.round(cx).astype(int)
            cy = np.round(cy).astype(int)
            cv2.circle(
                corners_img, (cx, cy), radius=4, color=[0, 0, 255], thickness=-1
            )

        debug_output_imgs.update(
            {
                "6-hough.jpg": hough_img,
                "7-intersections.jpg": intersects_img,
                "8-corners.jpg": corners_img,
            }
        )

    return four_corners_sort(centers)


def horizontal_vertical_edges_det(img, thresh_blurred, debug_output_imgs=None):
    cols = np.array(img).shape[1]
    horizontal_kernel_length = cols // 15
    rows = np.array(img).shape[0]
    vertical_kernel_length = rows // 15

    result = cv2.cvtColor(np.full_like(img, 0), cv2.COLOR_BGR2GRAY)
    vertical_kernel = cv2.getStructuringElement(
        cv2.MORPH_RECT, (1, vertical_kernel_length)
    )
    horizontal_kernel = cv2.getStructuringElement(
        cv2.MORPH_RECT, (horizontal_kernel_length, 1)
    )

    detect_vertical = cv2.morphologyEx(
        thresh_blurred, cv2.MORPH_OPEN, vertical_kernel, iterations=3
    )
    cnts = cv2.findContours(detect_vertical, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    for c in cnts:
        result = cv2.drawContours(result, [c], -1, (255, 255, 255), 2)

    detect_horizontal = cv2.morphologyEx(
        thresh_blurred, cv2.MORPH_OPEN, horizontal_kernel, iterations=3
    )
    cnts = cv2.findContours(
        detect_horizontal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
    )
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    for c in cnts:
        result = cv2.drawContours(result, [c], -1, (255, 255, 255), 2)

    if debug_output_imgs is not None:
        cv2.imwrite("debug_imgs/horizontal_vertical_edges.jpg", result)

    return result


def edges_det(img, min_val, max_val, debug_output_imgs=None):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray_filtered = cv2.bilateralFilter(gray, 9, 75, 75)
    thresh = cv2.threshold(gray_filtered, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[
        1
    ]
    thresh_blurred = cv2.medianBlur(thresh, 11)
    border_amount = 5
    img_border = cv2.copyMakeBorder(
        thresh_blurred,
        border_amount,
        border_amount,
        border_amount,
        border_amount,
        cv2.BORDER_CONSTANT,
        value=[0, 0, 0],
    )

    edges = cv2.Canny(img_border, min_val, max_val)
    dilation_amount = 6
    dilated = cv2.dilate(
        edges, np.ones((dilation_amount, dilation_amount), dtype=np.uint8)
    )

    if debug_output_imgs is not None:
        debug_output_imgs.update(
            {
                "1-edges_det_threshold.jpg": thresh_blurred,
                "2-edges_det_canny_dilated.jpg": dilated,
            }
        )

    total_border = border_amount + dilation_amount

    return dilated, total_border


def four_corners_sort(pts):
    diff = np.diff(pts, axis=1)
    summ = pts.sum(axis=1)
    return np.array(
        [
            pts[np.argmin(summ)],
            pts[np.argmax(diff)],
            pts[np.argmax(summ)],
            pts[np.argmin(diff)],
        ]
    )


def contour_offset(cnt, offset):
    cnt += offset
    cnt[cnt < 0] = 0
    return cnt


def straight_lines_in_contour(contour, delta=100):
    rectangle = True

    contour = four_corners_sort(contour)
    y_line1 = [contour[0], contour[1]]
    y_line2 = [contour[3], contour[2]]
    x_line1 = [contour[0], contour[3]]
    x_line2 = [contour[1], contour[2]]
    lines = [y_line1, y_line2, x_line1, x_line2]
    for (x1, y1), (x2, y2) in lines:
        if not (abs(x2 - x1) < delta or abs(y2 - y1) < delta):
            rectangle = False
            logger.debug("Contour is not a rectangle based on line straightness.")

    return rectangle


def find_page_contours(
    edges, img, border_size=11, min_area_mult=0.3, debug_output_imgs=None
):
    contours, hierarchy = cv2.findContours(
        edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
    )

    # Finding biggest rectangle otherwise return original corners
    height = edges.shape[0]
    width = edges.shape[1]
    MIN_COUNTOUR_AREA = height * width * min_area_mult
    MAX_COUNTOUR_AREA = (width - border_size - 5) * (height - border_size - 5)

    min_area = MIN_COUNTOUR_AREA
    page_contour = None
    none_tested = True
    for cnt in contours:
        perimeter = cv2.arcLength(cnt, True)
        approx = cv2.approxPolyDP(cnt, 0.1 * perimeter, True)

        _, _, w, h = cv2.boundingRect(approx)
        aspect_ratio = w / h
        if (
            len(approx) == 4
            and cv2.isContourConvex(approx)
            and min_area < cv2.contourArea(approx) < MAX_COUNTOUR_AREA
            and straight_lines_in_contour(approx[:, 0], delta=int(height / 10))
            and 0.9 < aspect_ratio < 3
        ):

            none_tested = False
            min_area = cv2.contourArea(approx)
            page_contour = approx[:, 0]

    if none_tested:
        logger.debug("No contours met the criteria.")
        return None
    page_contour = four_corners_sort(page_contour)

    if debug_output_imgs is not None:
        page_contour_img = img.copy()
        page_contour_img = cv2.drawContours(
            page_contour_img, [page_contour], -1, (0, 255, 0), 3
        )

        debug_output_imgs.update({"5-page_contour.jpg": page_contour_img})

    return contour_offset(page_contour, (-border_size, -border_size))


def persp_transform(img, s_points):
    height = max(
        np.linalg.norm(s_points[0] - s_points[1]),
        np.linalg.norm(s_points[2] - s_points[3]),
    )
    width = max(
        np.linalg.norm(s_points[1] - s_points[2]),
        np.linalg.norm(s_points[3] - s_points[0]),
    )

    t_points = np.array([[0, 0], [0, height], [width, height], [width, 0]], np.float32)
    if s_points.dtype != np.float32:
        s_points = s_points.astype(np.float32)

    M = cv2.getPerspectiveTransform(s_points, t_points)
    return cv2.warpPerspective(img, M, (int(width), int(height)))


def write_debug_imgs(debug_output_imgs, base_path="debug_imgs"):
    for filename, img_data in debug_output_imgs.items():
        save_path = os.path.join(base_path, filename)
        cv2.imwrite(save_path, img_data)


def crop(
    img_path,
    output_directory,
    mode="automatic",
    debug_output_imgs=False,
    save_debug_imgs=False,
    create_debug_gif=False,
    debug_gif_optimize=True,
    debug_path="debug_imgs",
    **kwargs
):
    if mode not in ["automatic", "contours", "hough_lines"]:
        raise AssertionError

    if not debug_output_imgs:
        debug_output_imgs = None
    else:
        debug_output_imgs = {}

    img = cv2.imread(img_path)

    edges_img, total_border = edges_det(img, 200, 50, debug_output_imgs)

    rows = np.array(img).shape[0]
    contour_removal_threshold = rows // 3
    edges_no_words = remove_contours(edges_img, contour_removal_threshold)

    edges_morphed = cv2.morphologyEx(edges_no_words, cv2.MORPH_CLOSE, np.ones((5, 11)))

    if debug_output_imgs is not None:
        debug_output_imgs.update(
            {
                "3-edges_no_words.jpg": edges_no_words,
                "4-edges_morphed.jpg": edges_morphed,
            }
        )

    if mode == "contours":
        page_contour = find_page_contours(
            edges_morphed, img, total_border, debug_output_imgs=debug_output_imgs
        )
        if page_contour is None:
            logger.warn(
                "Contours method failed. Using entire image. It is recommended to try 'automatic' mode."
            )
        corners = page_contour

    elif mode == "hough_lines":
        corners = hough_lines_corners(
            img,
            edges_morphed,
            contour_removal_threshold,
            total_border,
            debug_output_imgs,
        )

    elif mode == "automatic":
        page_contour = find_page_contours(
            edges_morphed, img, total_border, debug_output_imgs=debug_output_imgs
        )
        if page_contour is None:
            logger.debug("Contours method failed. Using `hough_lines_corners`.")
            corners = hough_lines_corners(
                img,
                edges_morphed,
                contour_removal_threshold,
                total_border,
                debug_output_imgs,
            )
        else:
            corners = page_contour

    if corners is not None:
        failed = False
        img_cropped = persp_transform(img, corners)
    else:
        failed = True
        img_cropped = img

    if not output_directory:
        output_directory = "C:\\Users\\HARSHITA KAMANI\\Desktop\\l2n\\frames_sorted_1\\slide"

    if debug_output_imgs is not None:
        debug_output_imgs.update({"9-img_cropped.jpg": img_cropped})

        if save_debug_imgs:
            write_debug_imgs(debug_output_imgs, base_path=debug_path)

        if create_debug_gif:
            scale_to = img.shape[:2]
            scale_to = (scale_to[1], scale_to[0])
            imgs_list = list(debug_output_imgs.values())
            for idx, img in enumerate(imgs_list[1:]):
                img = cv2.resize(img, scale_to)
                imgs_list[idx + 1] = img

            original_file_name = os.path.basename(str(img_path))
            filename = original_file_name + "_debug.gif"
            save_path = os.path.join(debug_path, filename)
            imageio.mimsave(save_path, imgs_list, duration=1.4)

            if debug_gif_optimize:
                optimize(save_path)

    file_parse = os.path.splitext(str(img_path))
    filename = file_parse[0]
    ext = file_parse[1]
    output_path = os.path.join(output_directory, filename + OUTPUT_PATH_MODIFIER + ext)
    cv2.imwrite(output_path, img_cropped)

    return output_path, failed

def all_in_folder(path, output_directory, remove_original=False, **kwargs):
    cropped_imgs_paths = []
    images = os.listdir(path)
    images.sort()
    for item in tqdm(
        images, total=len(images), desc="> Corner Crop Transform: Progress"
    ):
        current_path = os.path.join(path, item)
        if os.path.isfile(current_path) and OUTPUT_PATH_MODIFIER not in str(
            current_path
        ):
            output_path, failed = crop(current_path, output_directory=output_directory, **kwargs)

            if failed and "debug_output_imgs" in locals()["kwargs"]:
                logger.info("Failed: " + str(output_path))
                with open("debug_crop_error_log.txt", "a+") as error_log:
                    error_log.write(output_path + "\n")

            cropped_imgs_paths.append(output_path)
            if remove_original:
                os.remove(current_path)
    logger.debug("> Corner Crop Transform: Returning cropped image paths")
    return cropped_imgs_paths

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Perspective Crop to Rectangles (Slides)"
    )
    parser.add_argument(
        "mode",
        type=str,
        choices=["file", "folder"],
        help="`file` mode will crop a single image and `folder` mode will crop all the images in a given folder",
    )
    parser.add_argument(
        "path", type=str, help="path to file or folder (depending on `mode`) to process"
    )
    parser.add_argument(
        "-d",
        "--debug_mode",
        action="store_true",
        help="enable the usage of `--debug_imgs`, `--debug_gif`, and `--debug_path`.",
    )
    parser.add_argument(
        "-di",
        "--debug_imgs",
        action="store_true",
        help="Save debug images (JPG of each step of the pipeline). Requires `--debug_mode` to be enabled.",
    )
    parser.add_argument(
        "-dg",
        "--debug_gif",
        action="store_true",
        help="Save debug gif (GIF with 1.4s delay between each debug image). Requires `--debug_mode` to be enabled.",
    )
    parser.add_argument(
        "-dgo",
        "--debug_gif_optimize",
        action="store_true",
        help="Optimize the gif produced by enabling --debug_gif with `gifsicle`.",
    )
    parser.add_argument(
        "-p",
        "--debug_path",
        type=str,
        default="./debug_imgs",
        help="path to folder to store debug images (default: './debug_imgs')",
    )
    parser.add_argument(
        "-l",
        "--log",
        dest="logLevel",
        default="INFO",
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        help="Set the logging level (default: 'Info').",
    )

    args = parser.parse_args()

    if args.debug_gif and not args.debug_mode:
        parser.error("`--debug_gif` requires `--debug_mode` to be enabled.")
    if args.debug_imgs and not args.debug_mode:
        parser.error("`--debug_imgs` requires `--debug_mode` to be enabled.")

    make_dir_if_not_exist(args.debug_path)

    logging.basicConfig(
        format="%(asctime)s|%(name)s|%(levelname)s> %(message)s",
        level=logging.getLevelName(args.logLevel),
    )

    if args.mode == "file":
        crop(
            args.path,
            debug_output_imgs=args.debug_mode,
            save_debug_imgs=args.debug_imgs,
            create_debug_gif=args.debug_gif,
            debug_gif_optimize=args.debug_gif_optimize,
            debug_path=args.debug_path,
        )
    else:
        cropped_imgs_paths = all_in_folder(
            args.path,
            output_directory=r"C:\Users\HARSHITA KAMANI\Desktop\l2n\frames_sorted_1\slide",  # specify the output folder
            debug_output_imgs=args.debug_mode,
            save_debug_imgs=args.debug_imgs,
            create_debug_gif=args.debug_gif,
            debug_gif_optimize=args.debug_gif_optimize,
            debug_path=args.debug_path,
        )